import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserCredentials } from 'src/app/models/usercredentials';
import { Account } from 'src/app/models/account';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UrlService } from '../urlService/url.service';
import { map } from 'rxjs/operators';
import { Token } from '../../models/token';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  account: Account = new Account();
  private currentUserSubject: BehaviorSubject<Account>;
  public currentUser: Observable<Account>;

  constructor(private http: HttpClient, private urlService: UrlService) {
    this.currentUserSubject = new BehaviorSubject<Account>((this.convertTokenToUser()));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  register(username: string, password: string, fullname: string, email: string) {
    this.account.email = email;
    this.account.password = password;
    this.account.fullName = fullname;
    this.account.userName = username;
    this.account.bio = "New Kwetter user!"
    
    return this.http.post<any>(this.urlService.getRegisterUrl(), this.account)
      .pipe(map(data => {
        // login successful if there's a jwt token in the response
        if (data) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          console.log('tookie');
          localStorage.setItem('token', data);
          this.currentUserSubject.next(this.convertTokenToUser());
          console.log(this.convertTokenToUser());
        }
        console.log('Done');
        return this.convertTokenToUser();
      }));
  }

  convertTokenToUser(): Account {
    const account: Account = new Account();
    const token: Token = this.getDecodedAccessToken(localStorage.getItem('token'));
    if (token != null) {
      account.id = token.userId;
      account.fullName = token.fullName;
      account.email = token.email;
      account.userName = token.userName;
      account.bio = token.bio;
      return account;
    }
    return null;
  }

  getDecodedAccessToken(token: string): Token {
    try {
      return jwt_decode(token);
    } catch (Error) {
      console.log('Cannot decode token!');
      return null;
    }
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) { return null; }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  isTokenExpired(token?: string): boolean {
    if (!token) { token = this.getToken(); }
    if (!token) { return true; }

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) { return false; }
    return !(date.valueOf() > new Date().valueOf());
  }
}
