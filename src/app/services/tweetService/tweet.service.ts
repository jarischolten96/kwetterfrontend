import { Injectable } from '@angular/core';
import { Tweet } from 'src/app/models/tweet';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UrlService } from '../urlService/url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TweetService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  addTweet(tweet : Tweet): Observable<any> {
    return this.http.post<any>(this.urlService.getTweetUrl() + '/newTweet', tweet);  
  }

  getAllTweetsByUser(id:string): Observable<Tweet[]> {
    return this.http.get<Tweet[]>(this.urlService.getTweetUrl() + '/getTweetsByUser/' + id);
  }

}
