import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  private ipAddress = 'http://localhost:51447/kwetter_api';
  private loginUrl = '/user/login/';
  private registerUrl = '/user/register';
  private tweetUrl = '/tweet'
  private userUrl = '/user'

  constructor() { }

  getLoginUrl() {
    return this.ipAddress + this.loginUrl;
  }

  getRegisterUrl() {
    return this.ipAddress + this.registerUrl;
  }
 
  getTweetUrl() {
    return this.ipAddress + this.tweetUrl;
  }

  getUserUrl() {
    return this.ipAddress + this.userUrl;
  }
}
