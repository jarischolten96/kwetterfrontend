import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UrlService } from '../urlService/url.service';
import { Observable } from 'rxjs';
import { Account } from 'src/app/models/account';

@Injectable({
  providedIn: 'root'
})
export class DiscoverService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  getAllUsers(): Observable<Account[]> {
    return this.http.get<Account[]>(this.urlService.getUserUrl() + '/getAll');
  }
}
