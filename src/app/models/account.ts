import { Tweet } from './tweet';

export class Account {
  id: string;
  email: string;
  fullName: string;
  password: string;
  bio: string;
  userName:string;
  followers: Account[];
  following: Account[];
  tweets: Tweet[];
}
