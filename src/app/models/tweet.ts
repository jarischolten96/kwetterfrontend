import { Account } from './account'

export class Tweet {
    id:string
    user:Account
    text:string
    likes: Account[];
    date:Date;

}