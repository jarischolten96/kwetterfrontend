
export class Token {
    sub:string;
    email: string;
    exp: number;
    iss:string;
    fullName:string;
    userId:string;
    userName:string;
    bio:string;
}