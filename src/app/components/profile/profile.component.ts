import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/loginService/login.service'
import { Account } from 'src/app/models/account';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser: Account;

  constructor(private authenticationService: LoginService,private router: Router) 
  {
    this.currentUser = this.authenticationService.convertTokenToUser();
   }

  ngOnInit() {
  }

  logout() {
    this.currentUser = null;
    console.log('logout with nav!!!');
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
