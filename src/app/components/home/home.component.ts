import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/loginService/login.service'
import { Account } from 'src/app/models/account';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Tweet } from 'src/app/models/tweet';
import { TweetService } from '../../services/tweetService/tweet.service'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css','./home.component.scss']
})
export class HomeComponent implements OnInit {
  currentUser: Account;
  tweetForm: FormGroup;
  tweet: Tweet = new Tweet();

  obTweet$: Observable<Tweet[]>;
  tweets: Tweet[] = [];

  constructor( 
    private authenticationService: LoginService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private tweetService: TweetService) 
  {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.obTweet$ = this.tweetService.getAllTweetsByUser(this.currentUser.id);
    this.obTweet$.subscribe((data) => {
      this.tweets = data;
    });

    this.tweetForm = this.formBuilder.group({
      tweetText: ['', Validators.required],
  });
  }

  get f() { return this.tweetForm.controls; }

  onSubmit(){
    console.log(this.f.tweetText.value)
    console.log(new Date())
    console.log(this.currentUser)

    this.tweet.text = this.f.tweetText.value;
    this.tweet.date = new Date();
    this.tweet.user = this.currentUser;

    if (this.tweetForm.invalid) {
      return;
    }

    this.tweetService.addTweet(this.tweet).subscribe((data) => {
      if(data === 200){ 
        console.log("Tweet toegevoegd!")
        window.location.reload();
      } else {
        console.log("Tweet niet kunnen toevoegen!")
           }
    });
  }


  logout() {
    this.currentUser = null;
    console.log('logout with nav!!!');
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
