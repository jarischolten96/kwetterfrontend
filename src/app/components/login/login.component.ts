import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../services/loginService/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl = '';
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService
  ) {
      // redirect to home if already logged in
      if (this.loginService.convertTokenToUser()) {
        this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  async onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;

    console.log(this.f.username.value + this.f.password.value);

    try {
      let user = await this.loginService.login(this.f.username.value, this.f.password.value);
      this.loading = false;
      if (user.id != null) this.router.navigate([this.returnUrl]);
      
    } catch (error) {
          this.error = 'Name and/or password is not right!';
           this.loading = false;
    }
    

    // await this.loginService.login(this.f.username.value, this.f.password.value).subscribe(data => {
    //   this.loading = false;
    //   if (data.id != null) {
    //       this.router.navigate([this.returnUrl]);
    //   }
    //   return true;
    //   }, error => {
    //       this.error = 'Name and/or password is not right!';
    //       this.loading = false;
    //   }
  //);
  };

}
