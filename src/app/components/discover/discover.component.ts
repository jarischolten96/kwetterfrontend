import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from '../../services/loginService/login.service'
import { Account } from 'src/app/models/account';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Tweet } from 'src/app/models/tweet';
import { TweetService } from '../../services/tweetService/tweet.service'
import { DiscoverService } from '../../services/dicoverService/discover.service'

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.css']
})
export class DiscoverComponent implements OnInit {
  currentUser: Account;

  obUser$: Observable<Account[]>;
  users: Account[] = [];
  
  constructor(private authenticationService: LoginService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private discoverService: DiscoverService) 
    {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

  ngOnInit() {
    this.obUser$ = this.discoverService.getAllUsers();
    this.obUser$.subscribe((data) => {
      this.users = data;
    });

  }

  logout() {
    this.currentUser = null;
    console.log('logout with nav!!!');
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
