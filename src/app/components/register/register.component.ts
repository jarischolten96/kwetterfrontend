import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/loginService/login.service'
import { RegisterService } from '../../services/registerService/register.service'
import { Account } from 'src/app/models/account';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl = '';
  error = '';

  constructor(
    private authenticationService: LoginService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private registerService: RegisterService) {
    // redirect to home if already logged in
    if (this.registerService.convertTokenToUser()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.required],
      fullname: ['', Validators.required]
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    console.log(this.f.username.value + this.f.password.value + this.f.email.value + this.f.fullname.value);
    this.registerService.register(this.f.username.value, this.f.password.value, this.f.fullname.value, this.f.email.value).subscribe(data => {
      this.loading = false;
      if (data.id != null) {
        this.router.navigate([this.returnUrl]);
      }
      return true;
    }, error => {
      this.error = 'Info is not right!';
      this.loading = false;
    }
    );

  }
}
