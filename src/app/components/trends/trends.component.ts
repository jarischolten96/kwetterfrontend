import { Component, OnInit } from '@angular/core';
import { Account } from 'src/app/models/account';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../services/loginService/login.service'

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css']
})
export class TrendsComponent implements OnInit {
  currentUser: Account;
  
  constructor(private router: Router,
    private loginService: LoginService,private authenticationService: LoginService) { 
      this.currentUser = this.authenticationService.convertTokenToUser();
    }

  ngOnInit() {
  }

  logout() {
    this.currentUser = null;
    console.log('logout with nav!!!');
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
